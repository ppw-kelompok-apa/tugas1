from django import forms
from .models import AboutMessage

class AboutForm(forms.ModelForm):
    class Meta:
        model = AboutMessage
        fields = ['title', 'message']
        widgets = {
                   "title":forms.TextInput(attrs={'placeholder':'Name','name':'Name','class':'form-control'}),
                   "message":forms.TextInput(attrs={'placeholder':'description','name':'description','class':'form-control'}),
                  }  