from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from.models import AboutMessage
from.forms import AboutForm

# Create your views here.
def about(request):
    if request.user.is_authenticated:
        isLogin = True
    else:
        isLogin = False
    about_list = AboutMessage.objects.all()
    form = AboutForm()
    return render(request, 'about.html', {
        'isLogin': isLogin,
        'form': form,
        'about_list': about_list,
    })

def add_about_message(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = AboutForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.user = request.user
                obj.save()
                return redirect('about')