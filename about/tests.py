from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import auth
from django.contrib.auth.models import User
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from .models import AboutMessage
from .views import about

# Create your tests here.
class TestAbout(TestCase):
	def test_url_about_exist(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code, 200)

	def test_function_landing(self):
		found = resolve('/about/')
		self.assertEqual(found.func, about)

	def test_home(self):
		response = Client().get('/about/')
		self.assertTemplateUsed(response, 'about.html')
