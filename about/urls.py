from django.urls import path
from .views import about, add_about_message

urlpatterns = [
    path('', about, name='about'),
    path('post/', add_about_message, name='add_about_message'),
]
