from django.shortcuts import render
from django.contrib.auth.models import User
from donate.models import DonationProgram
from news.models import News

# Create your views here.
def home_page_view(request):
	user_length = User.objects.all().count()
	finished_program = 0
	total_donation = 0
	for program in DonationProgram.objects.all():
		if program.target_fund < program.current_fund:
			finished_program += 1
		total_donation += program.current_fund

	popular_program = DonationProgram.objects.order_by('-current_fund')[:3]
	count = popular_program.count()
	if count == 0:
		col = int(12 / 1)
	else:
		col = int(12 / popular_program.count())

	
	return render(request, 'baseapp/News.html', {'user_length': user_length, 'finished_program': finished_program
		, 'total_donation': total_donation, 'popular_program': popular_program, 'col': col})