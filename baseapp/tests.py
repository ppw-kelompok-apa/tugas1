from django.test import TestCase, Client
from donate.models import DonationProgram

# Create your tests here.
class BaseappTest(TestCase):
	def test_home_page(self):
		program = DonationProgram(program_name='test', target_fund=10000, current_fund=50000)
		program.save()
		response = Client().get('/')
		self.assertIn('50000', response.content.decode('utf8'))