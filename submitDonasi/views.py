from django.shortcuts import render
from django.shortcuts import render, redirect
from donate.models import DonationProgram
from django.contrib.auth.models import User
from .forms import register_form

# Create your views here.
def submit_donation(request, id):
	if request.user.is_authenticated:
		fund_amount = request.POST.get('fund_amount')
		if request.method == 'POST':
			user = request.user
			donationProgram = DonationProgram.objects.all().get(id=id)
			if donationProgram:
				donationProgram.current_fund += int(fund_amount)
				donationProgram.save()
				donationProgram.donator.add(user)
				user.donation_list.add(donationProgram)
			return redirect('detail_donasi', id=id)
		else:
			donationProgram = DonationProgram.objects.all().get(id=id)
			if donationProgram:
				form = register_form()
				return render(request, 'submitDonasi/submitDonation.html', {'form': form, 'program': donationProgram})
	else:
		return redirect('home')
