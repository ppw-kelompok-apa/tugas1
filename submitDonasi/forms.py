from django import forms
from django.core.validators import MinLengthValidator

from . import choices

class register_form(forms.Form):
    bank_choice = forms.ChoiceField(choices = choices.BANK_NAME, label="Nama Bank", 
    	initial='', widget=forms.Select(attrs={'class': 'form-control'}), required=True)
    account_number = forms.CharField(label='Nomor rekening', 
    	widget=forms.TextInput(attrs={'class': 'form-control'}))
    fund_amount = forms.IntegerField(label='Nominal donasi', 
    	widget=forms.TextInput(attrs={'class': 'form-control'}))