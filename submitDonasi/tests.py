from django.test import TestCase, Client
from django.contrib.auth.models import User
from donate.models import DonationProgram

# Create your tests here
class TestSubmitDonation(TestCase):
	def test_if_can_donate(self):
		user = User(username='testuser', first_name='testname')
		user.set_password('password')
		user.save()
		program = DonationProgram(program_name='test', target_fund=10000, current_fund=0)
		program.save()
		c = Client()
		c.login(username='testuser', password='password')
		response = c.post('/submit_donasi/1/', {'bank_choice': 'BIR', 'account_number': 12, 'fund_amount': '5000'})
		program = DonationProgram.objects.all().get(id=1)
		self.assertEquals(program.current_fund, 5000)

	def test_if_user_not_authenticated(self):
		program = DonationProgram(program_name='test', target_fund=10000, current_fund=0)
		program.save()
		c = Client()
		response = c.post('/submit_donasi/1/', {'bank_choice': 'BIR', 'account_number': 12, 'fund_amount': '5000'})
		program = DonationProgram.objects.all().get(id=1)
		self.assertEquals(program.current_fund, 0)

	def test_if_method_get(self):
		user = User(username='testuser', first_name='testname')
		user.set_password('password')
		user.save()
		program = DonationProgram(program_name='test', target_fund=10000, current_fund=0)
		program.save()
		c = Client()
		c.login(username='testuser', password='password')
		response = c.get('/submit_donasi/1/')
		self.assertTemplateUsed(response, 'submitDonasi/submitDonation.html')