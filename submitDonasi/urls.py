from django.urls import path

from . import views

#url for app, add your URL Configuration
urlpatterns = [
    path('<int:id>/', views.submit_donation, name='submit_donasi'),
]
