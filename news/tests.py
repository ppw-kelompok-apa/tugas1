from django.test import TestCase
from django.test import Client
from .models import News

# Create your tests here.
class NewsTest(TestCase):
	def test_all_news_in_newslist(self):
		news = News(title='test', image_link='none', content='lorem ipsum')
		news.save()
		news2 = News(title='test2', image_link='none', content='lorem ipsum')
		news2.save()
		response = Client().get('/news/newslist/')
		html_response = response.content.decode('utf8')
		self.assertIn('test', html_response)
		self.assertIn('test2', html_response)

	def test_news_is_in_html(self):
		news = News(title='test', image_link='none', content='lorem ipsum')
		news.save()
		response = Client().get('/news/1/')
		html_response = response.content.decode('utf8')
		self.assertIn('test', html_response)

	def test_new_name(self):
		news = News(title='test', image_link='none', content='lorem ipsum')
		news.save()
		self.assertEquals(str(news), news.title)
