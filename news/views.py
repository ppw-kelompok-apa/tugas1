from django.shortcuts import render
from .models import News

# Create your views here.
def news_detail(request, id):
	news = News.objects.all().get(id=id)
	return render(request, 'newsdetail.html', {'news': news})

def news_list(request):
	news = News.objects.all()
	return render(request, 'newslist.html', {'news': news})