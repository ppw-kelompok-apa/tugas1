from django.db import models
from donate.models import DonationProgram

# Create your models here.
class News(models.Model):
  title = models.CharField(max_length=255)
  image_link = models.CharField(max_length=255)
  content = models.TextField()
  published_date = models.DateTimeField(auto_now_add=True)
  program = models.OneToOneField(DonationProgram, on_delete=models.CASCADE, null=True)

  def __str__(self):
  	return self.title