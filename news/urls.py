from django.urls import path
from .views import news_detail, news_list

urlpatterns = [
	path('newslist/', news_list, name='newslist'),
	path('<int:id>/', news_detail, name='newsdetail'),
]