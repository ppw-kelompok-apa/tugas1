from django.shortcuts import render, redirect
from django.contrib.auth.models import User

# Create your views here.
def home_view(request):
	if request.user.is_authenticated:
		donation_list = request.user.donation_list.all()
	else:
		donation_list = []
	return render(request, 'account/home.html', {'user': request.user, 'donation_list': donation_list})