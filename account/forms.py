from django import forms
from django.core.validators import MinLengthValidator

class login_form(forms.Form):
	username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'class': 'form-control'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'minlength': '8'}))

class register_form(forms.Form):
	username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'class': 'form-control'}))
	nama_lengkap = forms.CharField(label='Nama Lengkap', widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.EmailField(label='E-Mail', widget=forms.EmailInput(attrs={'class': 'form-control'}))
	tanggal_lahir = forms.DateField(label='Tanggal Lahir', widget=forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'date'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'minlength': '8'}))
	confirm_password = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'minlength': '8'}))