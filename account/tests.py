from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.contrib import auth

# Create your tests here.
class WebTest(TestCase):
	def test_home(self):
		Client().get('/account/home/')
		self.assertTemplateUsed('account/home.html')