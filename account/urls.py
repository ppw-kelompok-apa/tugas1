from django.urls import path
from .views import home_view
from django.contrib.auth.views import LogoutView
from django.conf import settings

urlpatterns = [
	path('home/', home_view, name='home'),
	path('logout/', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL},
    	name='logout'),
]