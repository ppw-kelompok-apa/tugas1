from django.urls import path
from .views import detail_donasi


urlpatterns = [
	path('<int:id>/', detail_donasi, name='detail_donasi'),
]
