from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class DonationProgram(models.Model):
    program_name = models.CharField(max_length=255)
    started_time = models.DateTimeField(auto_now_add=True)
    donator = models.ManyToManyField(User, blank=True, related_name="donation_list")
    target_fund = models.IntegerField()
    current_fund = models.IntegerField(default=0)

    def __str__(self):
    	return self.program_name
