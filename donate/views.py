from django.shortcuts import render
from .models import DonationProgram

# Create your views here.
def detail_donasi(request, id):
	donationProgram = DonationProgram.objects.all().get(id=id)
	percent = int((donationProgram.current_fund / donationProgram.target_fund) * 100)
	return render(request, 'donation_detail.html', {'program': donationProgram, 'percent': percent})