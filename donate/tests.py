from django.test import TestCase, Client
from django.urls import resolve
from .views import detail_donasi
from .models import DonationProgram
from django.contrib.auth.models import User

# Create your tests here.
class Donate_Test(TestCase):
	def test_percent_is_right(self):
		program_name = 'test'
		target_fund = '20000'
		current_fund = '10000'
		program = DonationProgram(program_name=program_name, current_fund=current_fund, target_fund=target_fund)
		program.save()
		response = Client().get('/donate/1/')
		html_response = response.content.decode('utf8')
		self.assertIn('50%', html_response)

	def test_donate_str(self):
		program_name = 'test'
		target_fund = '20000'
		current_fund = '10000'
		program = DonationProgram(program_name=program_name, current_fund=current_fund, target_fund=target_fund)
		program.save()
		self.assertIn(str(program), program.program_name)
