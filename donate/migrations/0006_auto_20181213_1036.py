# Generated by Django 2.1.1 on 2018-12-13 03:36

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('donate', '0005_auto_20181020_1845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donationprogram',
            name='donator',
            field=models.ManyToManyField(blank=True, related_name='donation_list', to=settings.AUTH_USER_MODEL),
        ),
    ]
